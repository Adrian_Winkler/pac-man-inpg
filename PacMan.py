#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pygame
import time
# from math import sqrt
from random import seed, randint

seed()

pygame.init()
pygame.font.init()
GameFont = pygame.font.SysFont('dyuthi', 20)
GameFont2 = pygame.font.SysFont('liberationmono', 20, 1)
greeting = GameFont.render("READY!", 0, (255, 255, 0))
pygame.display.set_caption("PacMan")

# ********************************************************************************
#                               Cos jak makra
# ********************************************************************************
# define

WEST = -2
EAST = 1
NORTH = 2
SOUTH = -1

CHANGEDIR = 3
ROTATE = 4
MOVE = 5

CHASE = 6
SCATTER = 7
FRIGHTENED = 8

BLINKY = 9
PINKY = 10
INKY = 11
CLYDE = 12  # The retarded child

GAMEON = 235
HELP = 335
EXIT = 435
# hehe

# ********************************************************************************
#                                   CONFIG
# **************************************************************************

dbgpic = pygame.image.load("graphics/Kremowka.png")


def GameInit():
    global LEVEL, SPEED, GSModifier, PSModifier, BSModifier, FPS, modeTimer, frightenedTimer, counter, animStep, inkyWaitTill, youAreUselessClyde, mod1, mod2, mod3, frightime, deathBuffer, CheatMode
    LEVEL = 1  # duh
    SPEED = 2  # ilosc pixeli pokonywanych przy kazdym ruchu
    GSModifier = 3  # Ghosts i PacMan speed modifier. Mowi o tym co ktora klatke postac ma jedna pominac.
    PSModifier = 4  # Pozwala to ominac modyfikacje systemu ruchu z pixel-perfect na przedzialy
    # Teraz duchy gubią co 4 klatkę(75% predkosci) a Pac Man co 5 (80% predkosci)
    BSModifier = 3  # Modyfikator dla Blinkiego jest osobny bo zmienia się w trakcie poziomu
    FPS = 60  # duh

    modeTimer = 0  # liczy czas, wplywa na zmiane miedzy trybami CHASE - SCATTER
    frightenedTimer = 0  # liczy czas przerazenia duchow

    counter = 0  # Sluzy spowolnieniu animacji Pac-Mana
    animStep = 1  # Sluzy okresleniu klatki animacji Pac-Mana

    inkyWaitTill = 30  # Inky wyruszy dopiero po zdobyciu 30 punktow przez Pac-Mana (tak naprawde to jeszcze wplywa na to czas ale shhh)
    youAreUselessClyde = 60  # Jako ze to to samo co wyzej to podziele sie pewna refleksja... Clyde jest jak ten jeden czlowiek w grupie, ktory zachowuje sie jak najgrozniejszy, ale przychodzi co do czego to zaczyna uciekac i moze stanowic zagrozenie najwyzej gdy sie potknie, bo wtedy ktos moze sie potknac o niego.

    mod1 = 0
    mod2 = 0
    mod3 = 1  # mody(1,2,3) sluza do okreslania fazy duchow

    frightime = 5 * FPS  # heeeee... Ten, poza zabawna nazwa okresla granice czasu przerazenia

    deathBuffer = [0, 0, 3]

    CheatMode = False


# ********************************************************************************
#                                   MAPA
# ********************************************************************************
gridSize = 16

graph = [
    [(1, 11, EAST), (4, 3, NORTH)],  # 0
    [(0, 11, WEST), (2, 3, EAST), (8, 3, NORTH)],  # 1
    [(1, 3, WEST), (3, 11, EAST), (9, 3, NORTH)],  # 2
    [(2, 11, WEST), (13, 3, NORTH)],  # 3
    [(0, 3, SOUTH), (5, 2, EAST)],  # 4
    [(4, 2, WEST), (6, 3, EAST), (15, 3, NORTH)],  # 5
    [(5, 3, WEST), (16, 3, NORTH)],  # 6
    [(17, 3, NORTH), (8, 3, EAST)],  # 7
    [(1, 3, SOUTH), (7, 3, WEST)],  # 8
    [(2, 3, SOUTH), (10, 3, EAST)],  # 9
    [(9, 3, WEST), (20, 3, NORTH)],  # 10
    [(21, 3, NORTH), (12, 3, EAST)],  # 11
    [(11, 3, WEST), (22, 3, NORTH), (13, 2, EAST)],  # 12
    [(12, 2, WEST), (3, 3, SOUTH)],  # 13
    [(24, 3, NORTH), (15, 2, EAST)],  # 14
    [(14, 2, WEST), (5, 3, SOUTH)],  # 15
    [(6, 3, SOUTH), (25, 3, NORTH), (17, 3, EAST)],  # 16
    [(16, 3, WEST), (7, 3, SOUTH), (18, 3, EAST)],  # 17
    [(17, 3, WEST), (19, 3, EAST), (27, 3, NORTH)],  # 18
    [(18, 3, WEST), (20, 3, EAST), (28, 3, NORTH)],  # 19
    [(19, 3, WEST), (10, 3, SOUTH), (21, 3, EAST)],  # 20
    [(20, 3, WEST), (11, 3, SOUTH), (30, 3, NORTH)],  # 21
    [(12, 3, SOUTH), (23, 2, EAST)],  # 22
    [(22, 2, WEST), (31, 3, NORTH)],  # 23
    [(14, 3, SOUTH), (25, 5, EAST)],  # 24
    [(24, 5, WEST), (26, 3, EAST), (16, 3, SOUTH), (35, 6, NORTH)],  # 25
    [(25, 3, WEST), (27, 3, EAST), (32, 3, NORTH)],  # 26
    [(26, 3, WEST), (18, 3, SOUTH)],  # 27
    [(19, 3, SOUTH), (29, 3, EAST)],  # 28
    [(28, 3, WEST), (30, 3, EAST), (33, 3, NORTH)],  # 29
    [(29, 3, WEST), (31, 5, EAST), (21, 3, SOUTH), (38, 6, NORTH)],  # 30
    [(30, 5, WEST), (23, 3, SOUTH)],  # 31
    [(26, 3, SOUTH), (33, 9, EAST), (36, 3, NORTH)],  # 32
    [(32, 9, WEST), (29, 3, SOUTH), (37, 3, NORTH)],  # 33
    [(35, 6, EAST)],  # 34
    [(34, 6, WEST), (36, 3, EAST), (25, 6, SOUTH), (45, 6, NORTH)],  # 35
    [(35, 3, WEST), (32, 3, SOUTH), (40, 3, NORTH)],  # 36
    [(33, 3, SOUTH), (38, 3, EAST), (43, 3, NORTH)],  # 37
    [(37, 3, WEST), (39, 6, EAST), (30, 6, SOUTH), (50, 6, NORTH)],  # 38
    [(38, 6, WEST)],  # 39
    [(36, 3, SOUTH), (41, 3, EAST)],  # 40
    [(40, 3, WEST), (42, 3, EAST), (47, 3, NORTH)],  # 41
    [(41, 3, WEST), (43, 3, EAST), (48, 3, NORTH)],  # 42
    [(42, 3, WEST), (37, 3, SOUTH)],  # 43
    [(45, 5, EAST), (52, 3, NORTH)],  # 44
    [(44, 5, WEST), (35, 6, SOUTH), (53, 3, NORTH)],  # 45
    [(54, 3, NORTH), (47, 3, EAST)],  # 46
    [(46, 3, WEST), (41, 3, SOUTH)],  # 47
    [(42, 3, SOUTH), (49, 3, EAST)],  # 48
    [(48, 3, WEST), (57, 3, NORTH)],  # 49
    [(38, 6, SOUTH), (51, 5, EAST), (58, 3, NORTH)],  # 50
    [(50, 5, WEST), (59, 3, NORTH)],  # 51
    [(44, 3, SOUTH), (53, 5, EAST), (60, 4, NORTH)],  # 52
    [(52, 5, WEST), (54, 3, EAST), (45, 3, SOUTH), (61, 4, NORTH)],  # 53
    [(53, 3, WEST), (55, 3, EAST), (46, 3, SOUTH)],  # 54
    [(54, 3, WEST), (56, 3, EAST), (62, 4, NORTH)],  # 55
    [(55, 3, WEST), (57, 3, EAST), (63, 4, NORTH)],  # 56
    [(56, 3, WEST), (58, 3, EAST), (49, 3, SOUTH)],  # 57
    [(57, 3, WEST), (59, 5, EAST), (50, 3, SOUTH), (64, 4, NORTH)],  # 58
    [(58, 5, WEST), (51, 3, SOUTH), (65, 4, NORTH)],  # 59
    [(52, 4, SOUTH), (61, 5, EAST)],  # 60
    [(60, 5, WEST), (62, 6, EAST), (53, 4, SOUTH)],  # 61
    [(61, 6, WEST), (55, 4, SOUTH)],  # 62
    [(56, 4, SOUTH), (64, 6, EAST)],  # 63
    [(63, 6, WEST), (65, 5, EAST), (58, 4, SOUTH)],  # 64
    [(64, 5, WEST), (59, 4, SOUTH)],  # 65
]

coords = {
    0: [1, 31]
}  # slownik koordynatow. Uznalem, ze funkcji generujacej koordynaty na podstawie grafu latwiej bedzie sie poruszalo po tej strukturze

wasted = pygame.mixer.Sound("sounds/YouDied.wav")
dotsound = pygame.mixer.Sound("sounds/dot.wav")
startsound = pygame.mixer.Sound("sounds/Start.wav")
r_oof = pygame.mixer.Sound("sounds/of.wav")
pygame.mixer.music.load("sounds/Main_Theme_Loop.wav")
pygame.mixer.Sound.set_volume(wasted, 0.3)
pygame.mixer.Sound.set_volume(dotsound, 0.3)
# pygame.mixer.Sound.set_volume(startsound,0.3)

background = pygame.image.load("graphics/Map2.png")
logo = pygame.image.load("graphics/Logo.png")

bestscore = open("highscore", "r+")
oof = bestscore.readline().splitlines()
Highscore = GameFont2.render("HIGHSCORE: " + oof[0], 0, (255, 255, 255))

global GameScore
GameScore = GameFont2.render("0", 0, (255, 255, 255))

button1 = pygame.image.load("graphics/Button2.png")
button2 = pygame.image.load("graphics/Button1.png")
button3 = pygame.image.load("graphics/Button3.png")
bg_size = background.get_rect()
screen = pygame.display.set_mode((bg_size[2], bg_size[3]))
Starter = {
    ("NODE", 14): 2,
    ("NODE", 23): 2,
    ("NODE", 34): 0,
    (34, 34): [0, 0],
    ("NODE", 39): 0,
    (39, 39): [0, 0],
    ("NODE", 40): 0,
    ("NODE", 41): 0,
    ("NODE", 42): 0,
    ("NODE", 43): 0,
    ("NODE", 37): 0,
    ("NODE", 33): 0,
    ("NODE", 32): 0,
    ("NODE", 36): 0,
    (36, 40): [0, 0],
    (40, 41): [0, 0],
    (41, 42): [0, 0],
    (41, 47): [0, 0],
    (42, 43): [0, 0],
    (42, 48): [0, 0],
    (37, 43): [0, 0],
    (33, 37): [0, 0],
    (37, 38): [0, 0],
    (29, 33): [0, 0],
    (32, 33): [0, 0, 0, 0, 0, 0, 0, 0, 0],
    (32, 36): [0, 0],
    (26, 32): [0, 0],
    (35, 36): [0, 0],
    (34, 35): [0, 0, 0, 0, 0, 0],
    (38, 39): [0, 0, 0, 0, 0, 0],
    (18, 19): [0, 0],
    (52, 60): [1, 2, 1],
    (59, 65): [1, 2, 1],
}  # Obszary w ktorych nie maja byc generowane punkty. Przy przejsciu na kolejny poziom trzeba przywrocic ten stan planszy

WhereToFind = {

}  # fantastic points and where to find them hehe, tablica z info o punktach, na poczatku poziomu zawiera kopie startera


# ********************************************************************************
#                                KILKA FUNKCJI
# ********************************************************************************

def genCoords(node, xcoord, ycoord):  # Funkcja rekurencyjnie generujaca koordynaty
    for i in range(0, len(graph[node])):  # Dla kazdego sasiada danego wierzcholka
        if graph[node][i][0] not in coords:  # Jezeli nie sa znane jego koordynaty
            HowFar = graph[node][i][1]  # spisz odleglosc do niego
            if graph[node][i][
                2] == EAST:  # A nastepnie na podstawie zwrotu i kierunku wektora miedzy nimi ustal jego koordynaty i sprawdz koordynaty jego sasiadow
                coords[graph[node][i][0]] = [xcoord + HowFar, ycoord]
                genCoords(graph[node][i][0], xcoord + HowFar, ycoord)
            elif graph[node][i][2] == WEST:
                coords[graph[node][i][0]] = [xcoord - HowFar, ycoord]
                genCoords(graph[node][i][0], xcoord - HowFar, ycoord)
            elif graph[node][i][2] == SOUTH:
                coords[graph[node][i][0]] = [xcoord, ycoord + HowFar]
                genCoords(graph[node][i][0], xcoord, ycoord + HowFar)
            else:
                coords[graph[node][i][0]] = [xcoord, ycoord - HowFar]
                genCoords(graph[node][i][0], xcoord, ycoord - HowFar)


def initPoints(path):
    global amount
    global point
    amount = 0
    point = [pygame.image.load("graphics/PointTest.png"), pygame.image.load("graphics/UberPoint.png"),
             pygame.image.load("graphics/Bonus.png")]
    for i in range(0, len(graph)):
        if ("NODE", i) not in WhereToFind:
            WhereToFind[(
            "NODE", i)] = 1  # wierzcholki i krawedzie rozpatruje osobno, dla ulatwienia zmian wartosci (bez konfliktow)
            amount += 1
        for j in range(0, len(graph[i])):
            if (i, graph[i][j][0]) not in WhereToFind and (
            graph[i][j][0], i) not in WhereToFind:  # Jesli krawedz nie pojawila sie jeszcze w slowniku
                temp_Tab = []  # stworz tymczasowo tablice
                for k in range(0, graph[i][j][
                                      1] - 1):  # i powieksz o jedynke za kazdy kwadrat na siatce miedzy wierzcholkami
                    temp_Tab.append(1)
                    amount += 1
                WhereToFind[(i, graph[i][j][0])] = temp_Tab
                del temp_Tab
            # Numery wierzcholkow ustawione od lewej do prawej i od dolu do gory... Waaait... Dobra, odwroce to w koncowej wersji


# ////////////////////////////////////////////////////////////////////////////////////////////////////////

def setscr():
    screen.blit(background, bg_size)
    screen.blit(Highscore, (bg_size[2] / 2 - 80, 4))
    screen.blit(GameScore, (0, 4))

    for i in range(0, len(graph)):
        if (WhereToFind[("NODE", i)] == 1):
            screen.blit(point[0], (coords[i][0], coords[i][1]))
        if (WhereToFind[("NODE", i)] == 2):
            screen.blit(point[1], (coords[i][0], coords[i][1]))
        if (WhereToFind[("NODE", i)] == 3):
            screen.blit(point[2], (coords[i][0], coords[i][1]))
        for j in range(0, len(graph[i])):
            if (graph[i][j][2] == EAST):
                if (i, graph[i][j][0]) in WhereToFind:
                    for k in range(0, len(WhereToFind[(i, graph[i][j][0])])):
                        if (WhereToFind[(i, graph[i][j][0])][k] == 1):
                            screen.blit(point[0], (coords[i][0] + (k + 1) * gridSize, coords[i][1]))
                        if (WhereToFind[(i, graph[i][j][0])][k] == 2):
                            screen.blit(point[1], (coords[i][0] + (k + 1) * gridSize, coords[i][1]))
                        if (WhereToFind[(i, graph[i][j][0])][k] == 3):
                            screen.blit(point[2], (coords[i][0] + (k + 1) * gridSize, coords[i][1]))
            if (graph[i][j][2] == NORTH):  # ODWROC W KONCOWEJ WERSJI!
                if (i, graph[i][j][0]) in WhereToFind:
                    for k in range(0, len(WhereToFind[(i, graph[i][j][0])])):
                        if (WhereToFind[(i, graph[i][j][0])][k] == 1):
                            screen.blit(point[0], (coords[i][0], coords[i][1] - (k + 1) * gridSize))
                        if (WhereToFind[(i, graph[i][j][0])][k] == 2):
                            screen.blit(point[1], (coords[i][0], coords[i][1] - (k + 1) * gridSize))
                        if (WhereToFind[(i, graph[i][j][0])][k] == 3):
                            screen.blit(point[2], (coords[i][0], coords[i][1] - (k + 1) * gridSize))
                        # /////////////////////////////////////////////////////////////////////////////////////////////////////////


def SelectionSort(tab, ind):  # Pomaga w wyborze wierzcholka. Dalczego sortowanie? Odpowiedz jest prosta. Lenistwo.

    for i in range(0, len(tab)):
        least = [tab[i][ind], i]
        for j in range(i, len(tab)):
            if tab[j][ind] < least[0]:
                least[0] = tab[j][ind]
                least[1] = j
        tab[i], tab[least[1]] = tab[least[1]], tab[i]


# /////////////////////////////////////////////////////////////////////////////////////////////////////////

def setGhostMode(ghosts, mode):
    for i in ghosts:
        #        print(i.to_Node)
        if i.mode != FRIGHTENED:
            i.vec[0] *= (-1)
            i.vec[1] *= (-1)
            for j in range(len(graph[i.s_Node])):
                if graph[i.s_Node][j][0] == i.e_Node:
                    i.to_Node = graph[i.s_Node][j][1] * gridSize - i.to_Node
                    break
            i.e_Node, i.s_Node = i.s_Node, i.e_Node
            # if i.animStep==0:
            # i.character[i.animStep]=pygame.transform.flip(i.character[i.animStep], True, False)
            if i.animStep == 3:
                i.orientation = NORTH
                i.animStep = 2
            elif i.animStep == 2:
                i.orientation = SOUTH
                i.animStep = 3
            elif i.animStep == 1:
                i.orientation = EAST
                i.animStep = 0
            else:
                i.orientation = WEST
                i.animStep = 1

        if i.mode == FRIGHTENED:
            if not i.wasTouched:
                if i.orientation == WEST:
                    i.animStep = 1
                elif i.orientation == EAST:
                    i.animStep = 0
                elif i.orientation == SOUTH:
                    i.animStep = 3
                elif i.orientation == NORTH:
                    i.animStep = 2

        if mode == FRIGHTENED:
            i.animStep = 4
            i.wasFrightened = 1
            i.velocity = SPEED / 2

        i.mode = mode


# ********************************************************************************
#                                   KLASY
# ********************************************************************************

# PACMAN
class PacMan():
    def __init__(self, buffer, path, path2, path3):
        self.character = [pygame.image.load(path), pygame.image.load(path2), pygame.image.load(path3)]
        self.size = [self.character[0].get_rect(), self.character[1].get_rect(), self.character[2].get_rect()]
        self.position = [13 * gridSize, 24 * gridSize]
        self.first = True
        self.s_Node = 19
        self.e_Node = 18  # end node
        self.ioe_Node = 0  # index of e_Node, basically pomaga w szukaniu w grafie
        self.orientation = WEST
        self.to_Node = 24
        self.eaten = buffer[0]
        self.score = buffer[1]
        self.lives = buffer[2]
        self.eatTimer = 0  # liczy czas miedzy zjedzonymi punktami. Resetuje sie jak dojdzie do 4
        self.canMove = True
        self.to_Corner = 0
        self.alive = True
        self.velocity = SPEED
        self.wait = 0
        self.scalar = 0
        self.canI = True
        self.vector = [-1, 0,
                       "NONE"]  # Trzeci element informuje o tym, ktory element zmienil sie przy corneringu, oszczedza porownan

    # //////////////////////////////////////////////////////////////////////////////////

    def move(self, n, dirKey):
        PM.keyCallback(dirKey, MOVE)
        self.position[0] += (self.vector[0]) * self.canMove * self.velocity
        self.position[1] += (self.vector[1]) * self.canMove * self.velocity
        if (self.canMove):
            self.to_Node -= self.velocity
            if self.canI and (self.s_Node != 34 or self.e_Node != 34) and (self.s_Node != 39 or self.e_Node != 39) and \
                    graph[self.s_Node][self.ioe_Node][1] * gridSize - self.to_Node > 8: self.first = True
            if int(self.to_Corner) != 0:
                self.to_Corner -= self.scalar * self.velocity
                # print(self.to_Corner)
                if self.to_Corner == 0:
                    if self.vector[2] == "NONE":
                        print("O kurwa")
                    else:
                        self.vector[self.vector[2]] = 0  # Czyt - posprzataj po sobie
                    self.scalar = 0
                    self.vector[2] = "NONE"
                    # Tu pojawia się BUG
        self.draw(n)

    # //////////////////////////////////////////////////////////////////////////////////

    def nodification(self, dirKey):  # heeheeeee~
        # print(self.ioe_Node, self.s_Node)
        if (
                self.to_Node <= 8) and self.first:  # or (self.first and (graph[self.s_Node][self.ioe_Node][1]*gridSize-self.to_Node<7))):
            if self.orientation == NORTH or self.orientation == WEST:
                self.scalar = -1
            else:
                self.scalar = 1
            self.change = self.keyCallback(dirKey, ROTATE)
            if self.change:
                self.to_Corner = self.scalar * self.to_Node
                self.to_Node = 0
                self.eat()
                self.first = False


        elif (self.s_Node != 34 and self.e_Node != 34 and self.s_Node != 39 and self.e_Node != 39 and self.first and (
                graph[self.s_Node][self.ioe_Node][1] * gridSize - self.to_Node < 7)):
            if self.orientation == NORTH or self.orientation == WEST:
                self.scalar = 1
            else:
                self.scalar = -1
            self.buff = self.e_Node
            self.e_Node = self.s_Node
            self.change = self.keyCallback(dirKey, ROTATE)
            # print(self.change)
            # print(self.s_Node, self.e_Node)
            if self.change:
                self.to_Corner = self.scalar * (graph[self.s_Node][self.ioe_Node][1] * gridSize - self.to_Node)
                print("Chuj")
                self.to_Node = 0
                self.eat()
                self.first = False
            else:
                self.e_Node = self.buff

        if (self.to_Node == 0):
            if not self.change: self.change = self.keyCallback(dirKey, ROTATE)
            if self.e_Node == 34 and not self.change:
                if self.orientation == WEST:
                    self.to_Node = gridSize * 2
                    self.s_Node = 39
                    self.e_Node = 39
                elif self.orientation == EAST:
                    self.position[0] = coords[34][0] - (3 / 2) * gridSize
                    self.to_Node = graph[self.e_Node][0][1] * gridSize + gridSize
                    self.e_Node = 35
                    self.s_Node = 34
                    self.canI = False
                    self.ioe_Node = 0
            elif self.e_Node == 39 and not self.change:
                if self.orientation == EAST:
                    self.to_Node = gridSize * 2
                    self.s_Node = 34
                    self.e_Node = 34
                elif self.orientation == WEST:
                    self.position[0] = coords[39][0] + (1 / 2) * gridSize  # wynika to z wymiarow pacmana
                    self.to_Node = graph[self.e_Node][0][1] * gridSize + gridSize
                    self.e_Node = 38
                    self.s_Node = 39
                    self.canI = False
                    self.ioe_Node = 0
            else:
                for i in range(0, len(graph[self.e_Node])):
                    if (graph[self.e_Node][i][2] == self.orientation and graph[self.e_Node][i][0] != self.s_Node):
                        # self.ancestor = self.s_Node
                        self.s_Node = self.e_Node
                        self.e_Node = graph[self.e_Node][i][0]
                        self.to_Node = graph[self.s_Node][i][1] * gridSize
                        self.canMove = True
                        if self.change == False: self.first = True
                        self.ioe_Node = i
                        self.change = False
                        break
                    elif (i == len(graph[self.e_Node]) - 1):
                        self.canMove = 0
                        self.s_Node = self.e_Node
            self.change = False
        elif (self.canI and (self.s_Node != 34 or self.e_Node != 34) and (self.s_Node != 39 or self.e_Node != 39)):
            self.keyCallback(dirKey, CHANGEDIR)
        elif (not self.canI and ([self.position[0] + (1 / 2) * gridSize, self.position[1] + gridSize] == coords[34] or [
            self.position[0] + (1 / 2) * gridSize, self.position[1] + gridSize] == coords[39])):
            self.canI = True

    # //////////////////////////////////////////////////////////////////////////////////

    def charRot(self, degrees):  # obraca wszystkie klatki animacji o stopien. Cel: Mniejszy burdel w innych funkcjach
        self.character[0] = pygame.transform.rotate(self.character[0], degrees)
        self.character[1] = pygame.transform.rotate(self.character[1], degrees)
        self.character[2] = pygame.transform.rotate(self.character[2], degrees)

    # //////////////////////////////////////////////////////////////////////////////////

    def keyCallback(self, key, flag):
        global CheatMode
        if (key == pygame.K_ESCAPE):
            pygame.quit()
        elif (key == pygame.K_BACKQUOTE):
            if CheatMode:
                CheatMode = False
            else:
                CheatMode = True
            # print(CheatMode)
        elif (CheatMode and key == pygame.K_e):
            setGhostMode(ALLGHOSTS, SCATTER)
        elif (CheatMode and key == pygame.K_q):
            setGhostMode(ALLGHOSTS, CHASE)
        elif (CheatMode and key == pygame.K_SPACE):
            time.sleep(1)
        elif (CheatMode and key == pygame.K_r):  # ZA WAAAARUDOOOO
            if Blinky.canMove:
                Blinky.canMove = 0
                Inky.canMove = 0
                Pinky.canMove = 0
                Clyde.canMove = 0
            else:
                Blinky.canMove = 1
                Inky.canMove = 1
                Pinky.canMove = 1
                Clyde.canMove = 1

        elif (key == pygame.K_w or key == pygame.K_UP):
            if (flag == CHANGEDIR):
                if self.orientation == SOUTH and self.to_Corner == 0:
                    self.to_Node = graph[self.s_Node][self.ioe_Node][1] * gridSize - self.to_Node
                    self.e_Node, self.s_Node = self.s_Node, self.e_Node
                    for i in range(len(graph[self.s_Node])):
                        if graph[self.s_Node][i][0] == self.e_Node:
                            self.ioe_Node = i
                            break
                    self.orientation = NORTH
                    self.charRot(180)
            if (flag == MOVE and self.orientation == NORTH):
                self.vector[0] = 0
                self.vector[1] = -1
                if self.to_Corner != 0:
                    self.vector[0] = int(abs(self.to_Corner) / self.to_Corner)
                    self.vector[2] = 0
            if (flag == ROTATE):
                for i in range(0, len(graph[self.e_Node])):
                    if (graph[self.e_Node][i][2] == NORTH):
                        if self.orientation == EAST:
                            self.charRot(90)
                            self.position[0] -= 8
                            self.position[1] += 8
                        elif self.orientation == WEST:
                            self.charRot(270)
                            self.position[0] -= 8
                            self.position[1] += 8
                        elif self.orientation == SOUTH:
                            if self.to_Node == 0:
                                self.orientation = NORTH
                                self.charRot(180)
                            return False
                        elif self.orientation == NORTH:
                            return False
                        self.orientation = NORTH
                        self.first = False
                        return True

        elif (key == pygame.K_s or key == pygame.K_DOWN):
            if (flag == CHANGEDIR):
                if self.orientation == NORTH and self.to_Corner == 0:
                    self.to_Node = graph[self.s_Node][self.ioe_Node][1] * gridSize - self.to_Node
                    self.e_Node, self.s_Node = self.s_Node, self.e_Node
                    for i in range(len(graph[self.s_Node])):
                        if graph[self.s_Node][i][0] == self.e_Node:
                            self.ioe_Node = i
                            break
                    self.orientation = SOUTH
                    self.charRot(180)
            if (flag == MOVE and self.orientation == SOUTH):
                self.vector[0] = 0
                self.vector[1] = 1
                if self.to_Corner != 0:
                    self.vector[0] = int(abs(self.to_Corner) / self.to_Corner)
                    self.vector[2] = 0
            if (flag == ROTATE):
                for i in range(0, len(graph[self.e_Node])):
                    if (graph[self.e_Node][i][2] == SOUTH):
                        if self.orientation == EAST:
                            self.charRot(270)
                            self.position[0] -= 8
                            self.position[1] += 8
                        elif self.orientation == WEST:
                            self.charRot(90)
                            self.position[0] -= 8
                            self.position[1] += 8
                        elif self.orientation == NORTH:
                            if self.to_Node == 0:
                                self.orientation = SOUTH
                                self.charRot(180)
                            return False
                        elif self.orientation == SOUTH:
                            return False
                        self.orientation = SOUTH
                        self.first = False
                        return True
        elif (key == pygame.K_d or key == pygame.K_RIGHT):
            if (flag == CHANGEDIR):
                if self.orientation == WEST and self.to_Corner == 0:
                    self.to_Node = graph[self.s_Node][self.ioe_Node][1] * gridSize - self.to_Node
                    self.e_Node, self.s_Node = self.s_Node, self.e_Node
                    for i in range(len(graph[self.s_Node])):
                        if graph[self.s_Node][i][0] == self.e_Node:
                            self.ioe_Node = i
                            break
                    self.orientation = EAST
                    self.charRot(180)
            if (flag == MOVE and PM.orientation == EAST):
                self.vector[0] = 1
                self.vector[1] = 0
                if self.to_Corner != 0:
                    self.vector[1] = int(abs(self.to_Corner) / self.to_Corner)
                    self.vector[2] = 1
            if (flag == ROTATE):
                for i in range(0, len(graph[self.e_Node])):
                    if (graph[self.e_Node][i][2] == EAST):
                        if self.orientation == NORTH:
                            self.charRot(270)
                            self.position[0] += 8
                            self.position[1] -= 8
                        elif self.orientation == SOUTH:
                            self.charRot(90)
                            self.position[0] += 8
                            self.position[1] -= 8
                        elif self.orientation == WEST:
                            if self.to_Node == 0:
                                self.charRot(180)
                                self.orientation = EAST
                            return False
                        elif self.orientation == EAST:
                            return False
                        self.orientation = EAST
                        self.first = False
                        return True
        elif (key == pygame.K_a or key == pygame.K_LEFT):
            if (flag == CHANGEDIR):
                if self.orientation == EAST and self.to_Corner == 0:
                    self.to_Node = graph[self.s_Node][self.ioe_Node][1] * gridSize - self.to_Node
                    self.e_Node, self.s_Node = self.s_Node, self.e_Node
                    for i in range(len(graph[self.s_Node])):
                        if graph[self.s_Node][i][0] == self.e_Node:
                            self.ioe_Node = i
                            break
                    self.orientation = WEST
                    self.charRot(180)
            if (flag == MOVE and PM.orientation == WEST):
                self.vector[0] = -1
                self.vector[1] = 0
                if self.to_Corner != 0:
                    self.vector[1] = int(abs(self.to_Corner) / self.to_Corner)
                    self.vector[2] = 1
            if (flag == ROTATE):
                for i in range(0, len(graph[self.e_Node])):
                    if (graph[self.e_Node][i][2] == WEST):
                        if self.orientation == NORTH:
                            self.charRot(90)
                            self.position[0] += 8
                            self.position[1] -= 8
                        elif self.orientation == SOUTH:
                            self.charRot(270)
                            self.position[0] += 8
                            self.position[1] -= 8
                        elif self.orientation == EAST:
                            if self.to_Node == 0:
                                self.orientation = WEST
                                self.charRot(180)
                            return False
                        elif self.orientation == WEST:
                            return False
                        self.orientation = WEST
                        self.first = False
                        return True

    # //////////////////////////////////////////////////////////////////////////////////

    def draw(self, n):
        screen.blit(self.character[n], (self.position[0], self.position[1]))

    # //////////////////////////////////////////////////////////////////////////////////

    def eat(self):
        global GameScore
        if self.to_Node == 0:
            if WhereToFind[
                ("NODE", self.e_Node)] == 1:  # takie ustawienie konieczne, zeby nastepny elif dzialal jak trzeba
                WhereToFind[("NODE", self.e_Node)] = 0
                pygame.mixer.Sound.play(dotsound)
                self.score += 10
                self.eaten += 1
                self.wait = 1
                self.eatTimer = 0

            elif WhereToFind[
                ("NODE", self.e_Node)] == 2:  # takie ustawienie konieczne, zeby nastepny elif dzialal jak trzeba
                WhereToFind[("NODE", self.e_Node)] = 0
                pygame.mixer.Sound.play(dotsound)
                self.score += 50
                self.eaten += 1
                self.wait = 3
                setGhostMode(ALLGHOSTS, FRIGHTENED)
                self.eatTimer = 0


        elif self.to_Node % gridSize == 0:  # ODWROC W KONCOWEJ WERSJI!
            if self.orientation == NORTH:
                self.temp_Pos = len(WhereToFind[(self.s_Node, self.e_Node)]) - (int)(self.to_Node / gridSize)
                if WhereToFind[(self.s_Node, self.e_Node)][
                    self.temp_Pos] == 1:  # ODWROC s_Node z e_Node W KONCOWEJ WERSJI!
                    WhereToFind[(self.s_Node, self.e_Node)][self.temp_Pos] = 0
                    pygame.mixer.Sound.play(dotsound)
                    self.score += 10
                    self.eaten += 1
                    self.wait = 1

                elif WhereToFind[(self.s_Node, self.e_Node)][
                    self.temp_Pos] == 2:  # ODWROC s_Node z e_Node W KONCOWEJ WERSJI!
                    WhereToFind[(self.s_Node, self.e_Node)][self.temp_Pos] = 0
                    pygame.mixer.Sound.play(dotsound)
                    self.score += 50
                    setGhostMode(ALLGHOSTS, FRIGHTENED)
                    self.eaten += 1
                    self.wait = 3

            elif self.orientation == SOUTH:
                self.temp_Pos = (int)(self.to_Node / gridSize) - 1
                if WhereToFind[(self.e_Node, self.s_Node)][
                    self.temp_Pos] == 1:  # ODWROC s_Node z e_Node W KONCOWEJ WERSJI!
                    WhereToFind[(self.e_Node, self.s_Node)][self.temp_Pos] = 0
                    pygame.mixer.Sound.play(dotsound)
                    self.score += 10
                    self.eaten += 1
                    self.wait = 1

                elif WhereToFind[(self.e_Node, self.s_Node)][
                    self.temp_Pos] == 2:  # ODWROC s_Node z e_Node W KONCOWEJ WERSJI!
                    WhereToFind[(self.e_Node, self.s_Node)][self.temp_Pos] = 0
                    pygame.mixer.Sound.play(dotsound)
                    self.score += 50
                    setGhostMode(ALLGHOSTS, FRIGHTENED)
                    self.eaten += 1
                    self.wait = 3

            elif self.orientation == EAST:
                self.temp_Pos = len(WhereToFind[(self.s_Node, self.e_Node)]) - (int)(self.to_Node / gridSize)
                if WhereToFind[(self.s_Node, self.e_Node)][
                    self.temp_Pos] == 1:  # ODWROC s_Node z e_Node W KONCOWEJ WERSJI!
                    WhereToFind[(self.s_Node, self.e_Node)][self.temp_Pos] = 0
                    pygame.mixer.Sound.play(dotsound)
                    self.score += 10
                    self.eaten += 1
                    self.wait = 1

                elif WhereToFind[(self.s_Node, self.e_Node)][
                    self.temp_Pos] == 2:  # ODWROC s_Node z e_Node W KONCOWEJ WERSJI!
                    WhereToFind[(self.s_Node, self.e_Node)][self.temp_Pos] = 0
                    pygame.mixer.Sound.play(dotsound)
                    self.score += 50
                    setGhostMode(ALLGHOSTS, FRIGHTENED)
                    self.eaten += 1
                    self.wait = 3

            elif self.orientation == WEST:
                self.temp_Pos = (int)(self.to_Node / gridSize) - 1
                if WhereToFind[(self.e_Node, self.s_Node)][self.temp_Pos] == 1 or \
                        WhereToFind[(self.e_Node, self.s_Node)][
                            self.temp_Pos] == 2:  # ODWROC s_Node z e_Node W KONCOWEJ WERSJI!
                    WhereToFind[(self.e_Node, self.s_Node)][self.temp_Pos] = 0
                    pygame.mixer.Sound.play(dotsound)
                    self.wait = 1
                    self.score += 10
                    self.eaten += 1

                elif WhereToFind[(self.e_Node, self.s_Node)][self.temp_Pos] == 2:
                    WhereToFind[(self.e_Node, self.s_Node)][self.temp_Pos] = 0
                    self.wait = 3
                    self.score += 50
                    setGhostMode(ALLGHOSTS, FRIGHTENED)
                    self.eaten += 1
                    pygame.mixer.Sound.play(dotsound)

        GameScore = GameFont2.render(str(self.score), 0, (255, 255, 255))
        global oof, Highscore
        if self.score > int(oof[0]):
            Highscore = GameFont2.render("HIGHSCORE: " + str(self.score), 0, (255, 255, 255))


# //////////////////////////////////////////////////////////////////////////////////

class Ghost:
    def __init__(self, SpeedModifier, p_side_right, p_side_left, p_up, p_down, typeofghost):
        self.character = [pygame.image.load(p_side_right), pygame.image.load(p_side_left), pygame.image.load(p_up),
                          pygame.image.load(p_down), pygame.image.load("graphics/Frightened1.png"),
                          pygame.image.load("graphics/Frightened2.png"), pygame.image.load("graphics/Touched.png")]
        self.vec = [-1, 0]
        if typeofghost == BLINKY:
            self.toGo = [0, 0]
            self.corner = 59
            self.out = True
            # self.position = [coords[41][0]+gridSize, coords[41][1]-gridSize]
        if typeofghost == PINKY:
            self.toGo = [0, -3 * gridSize]
            self.corner = 60
            self.out = False
        if typeofghost == INKY:
            self.toGo = [2 * gridSize, -3 * gridSize]
            self.corner = 3
            self.out = False
        if typeofghost == CLYDE:  # the retarded child
            self.toGo = [-2 * gridSize, -3 * gridSize]
            self.corner = 0
            self.out = False
        self.position = [coords[41][0] + gridSize - self.toGo[0], coords[41][1] - gridSize - self.toGo[1]]
        self.personality = typeofghost
        self.s_Node = 42
        self.e_Node = 41
        self.to_Node = (3 / 2) * gridSize
        self.canMove = 1
        self.canGetOut = False
        self.orientation = WEST
        self.targetTile = 0
        self.velocity = SPEED
        self.wasTouched = 0
        self.wasFrightened = 0
        self.wait = 0
        self.animStep = 1
        self.SpeedModifier = SpeedModifier
        # self.inTunnel = 0
        #        self.path = []
        self.WhereIs = 0  # this motherf...
        if PM.lives == 3:
            self.mode = SCATTER
        else:
            self.mode = CHASE

    #    def getDist(self, begin, end):
    #        if self.mode == CHASE:
    #            a = pow(coords[end][0]-coords[begin][0],2)
    #            b = pow(coords[end][1]-coords[begin][1],2)
    # basically dlugosci wektorow...
    #           return sqrt(a+b)
    #       if self.mode == SCATTER:
    #           return self.corner #Leci na swoj kacik
    def draw(self):
        screen.blit(self.character[self.animStep], (self.position[0], self.position[1]))

    def move(self, n, byPoints, byTime):
        if self.toGo == [0, 0]:
            if self.wasTouched and self.to_Node % 4 == 0: self.velocity = 2 * SPEED
            if self.wasFrightened and self.mode != FRIGHTENED and self.to_Node % 4 == 0:
                self.velocity = SPEED
                self.wasFrightened = 0
            self.position[0] += (self.vec[0]) * self.canMove * self.velocity
            self.position[1] += (self.vec[1]) * self.canMove * self.velocity
            if (self.canMove): self.to_Node -= self.velocity
            # self.draw(n)
            if self.wasTouched and self.position == [coords[41][0] - (1 / 2) * gridSize, coords[41][1] - gridSize]:
                self.toGo = [(3 / 2) * gridSize, 3 * gridSize]

            elif (PM.orientation == NORTH or PM.orientation == SOUTH):
                if self.position[0] - (1 / 2) * gridSize >= PM.position[0] - (1 / 4) * gridSize and self.position[0] - (
                        1 / 2) * gridSize <= PM.position[0] + (1 / 4) * gridSize:
                    if self.position[1] + (1 / 2) * gridSize >= PM.position[1] - (1 / 4) * gridSize and self.position[
                        1] + (1 / 2) * gridSize <= PM.position[1] + (1 / 4) * gridSize:
                        if self.mode == FRIGHTENED:
                            self.targetTile = coords[41]
                            self.wasTouched = 1
                            self.animStep = 6
                            # self.velocity = 4
                        elif self.wasTouched == 0:
                            PM.alive = False

            elif (PM.orientation == WEST or PM.orientation == EAST):
                if self.position[0] >= PM.position[0] - (1 / 4) * gridSize and self.position[0] <= PM.position[0] + (
                        1 / 4) * gridSize:
                    if self.position[1] >= PM.position[1] - (1 / 4) * gridSize and self.position[1] <= PM.position[
                        1] + (1 / 4) * gridSize:
                        if self.mode == FRIGHTENED:
                            self.targetTile = coords[41]
                            self.animStep = 6
                            self.wasTouched = 1
                            # self.velocity = 4

                        elif self.wasTouched == 0:
                            PM.alive = False

        elif byPoints or byTime or self.canGetOut:  # CanGetOutByPoints or Time
            if byTime: self.canGetOut = True
            self.velocity = SPEED / 2
            if self.toGo[0] != 0:
                self.vec[0] = int(self.toGo[0] / abs(self.toGo[0]))
                self.vec[1] = 0
                self.position[0] += self.vec[0] * self.canMove * self.velocity
                self.toGo[0] -= self.vec[0] * self.canMove * self.velocity
            elif self.toGo[1] != 0:
                self.vec[0] = 0
                self.vec[1] = self.toGo[1] / abs(self.toGo[1])
                self.position[1] += self.vec[1] * self.canMove * self.velocity
                self.toGo[1] -= self.vec[1] * self.canMove * self.velocity
            if self.toGo == [0, 0]:
                if self.wasTouched == 1:  # WHOOP WHOOP NAPISZ W MAINIE COS O FRIGHTENED`
                    self.wasTouched = 0  # Do mnie sprzed tygodnia: Dzieki za komentarz powyzej leniwy cwelu
                    self.animStep = 2
                    self.mode = CHASE
                    self.toGo = [0, -3 * gridSize]

                else:
                    self.velocity = SPEED
                    self.s_Node = 42
                    self.e_Node = 41
                    self.to_Node = (3 / 2) * gridSize
                    self.vec[0] = -1
                    self.vec[1] = 0
                    self.out = True
                    PM.eatTimer = 0
        self.draw()

    def toTarget(self, s_Point):
        if not self.wasTouched:  # Ten warunek zapewnia, ze dotrze do GhostHouse'u nawet jesli skonczy sie Frightened Mode
            if self.mode == CHASE:
                if self.personality == BLINKY:
                    self.targetTile = PM.position

                if self.personality == PINKY:
                    if PM.orientation == NORTH:
                        self.targetTile = [PM.position[0], PM.position[1] - 4 * gridSize]
                    elif PM.orientation == SOUTH:
                        self.targetTile = [PM.position[0], PM.position[1] + 4 * gridSize]
                    elif PM.orientation == WEST:
                        self.targetTile = [PM.position[0] - 4 * gridSize, PM.position[1]]
                    elif PM.orientation == EAST:
                        self.targetTile = [PM.position[0] + 4 * gridSize, PM.position[1]]

                if self.personality == INKY:  # Should work
                    if PM.orientation == NORTH:
                        self.targetTile = [Blinky.position[0] + (PM.position[0] - Blinky.position[0]) * 2,
                                           Blinky.position[1] + (
                                                   PM.position[1] - 2 * gridSize - Blinky.position[1]) * 2]
                    elif PM.orientation == SOUTH:
                        self.targetTile = [Blinky.position[0] + (PM.position[0] - Blinky.position[0]) * 2,
                                           Blinky.position[1] + (
                                                   PM.position[1] + 2 * gridSize - Blinky.position[1]) * 2]
                    elif PM.orientation == WEST:
                        self.targetTile = [
                            Blinky.position[0] + (PM.position[0] - 2 * gridSize - Blinky.position[0]) * 2,
                            Blinky.position[1] + (PM.position[1] - Blinky.position[1]) * 2]
                    elif PM.orientation == EAST:
                        self.targetTile = [
                            Blinky.position[0] + (PM.position[0] + 2 * gridSize - Blinky.position[0]) * 2,
                            Blinky.position[1] + (PM.position[1] - Blinky.position[1]) * 2]
                    screen.blit(dbgpic, (self.targetTile[0] + gridSize, self.targetTile[1] + gridSize))

                if self.personality == CLYDE:
                    if pow(PM.position[0] - self.position[0], 2) + pow(PM.position[1] - self.position[1],
                                                                       2) > 64 * gridSize * gridSize:
                        self.targetTile = PM.position
                    else:
                        self.targetTile = coords[0]
            elif self.mode == SCATTER:
                self.targetTile = coords[self.corner].copy()
                if self.personality == BLINKY:
                    global bg_size
                    self.targetTile = [bg_size[2] - 4 * gridSize, 0]

        return pow(self.targetTile[0] - s_Point[0], 2) + pow(self.targetTile[1] - s_Point[1], 2)

    def nodification(self):
        if self.to_Node == 0:
            if self.e_Node == 34:
                if self.vec[0] == -1:
                    self.to_Node = gridSize * 2
                    self.s_Node = 39
                    self.e_Node = 39
                elif self.vec[0] == 1:
                    self.position[0] = coords[34][0] - (3 / 2) * gridSize
                    self.to_Node = graph[self.e_Node][0][1] * gridSize + gridSize
                    self.e_Node = 35
                    self.s_Node = 34
            elif self.e_Node == 39:
                if self.vec[0] == 1:
                    self.to_Node = gridSize * 2
                    self.s_Node = 34
                    self.e_Node = 34
                elif self.vec[0] == -1:
                    self.position[0] = coords[39][0] + (1 / 2) * gridSize  # wynika to z wymiarow pacmana
                    self.to_Node = graph[self.e_Node][0][1] * gridSize + gridSize
                    self.e_Node = 38
                    self.s_Node = 39

            elif self.mode == FRIGHTENED and not self.wasTouched:  # Nie liczy sie po zlapaniu
                if (self.s_Node == 34 and self.e_Node == 35) or (
                        self.s_Node == 39 and self.e_Node == 38): self.velocity = SPEED  # 2 Tu bylo*=2 nie wiem czy nie zjebalem. #To sluzy chyba do wyjscia z tunelu, w sumie nie pamietam
                self.buff = self.s_Node
                self.RNG = randint(0, len(graph[self.e_Node]) - 1)
                if graph[self.e_Node][self.RNG][0] == self.s_Node:
                    if self.RNG == len(graph[self.e_Node]) - 1:
                        self.RNG = len(graph[self.e_Node]) - 2
                    else:
                        self.RNG = len(graph[self.e_Node]) - 1
                self.s_Node = self.e_Node
                self.to_Node = graph[self.e_Node][self.RNG][1] * gridSize
                self.e_Node = graph[self.e_Node][self.RNG][0]
                if graph[self.s_Node][self.RNG][2] == NORTH:
                    self.orientation = NORTH
                    self.vec[0] = 0
                    self.vec[1] = -1
                elif graph[self.s_Node][self.RNG][2] == SOUTH:
                    self.orientation = SOUTH
                    self.vec[0] = 0
                    self.vec[1] = 1
                elif graph[self.s_Node][self.RNG][2] == WEST:
                    self.orientation = WEST
                    self.vec[0] = -1
                    self.vec[1] = 0
                elif graph[self.s_Node][self.RNG][2] == EAST:
                    self.orientation = EAST
                    self.vec[0] = 1
                    self.vec[1] = 0
                if (self.s_Node == 35 and self.e_Node == 34) or (
                        self.s_Node == 38 and self.e_Node == 39): self.velocity = SPEED / 2  # tu bylo /=2


            else:  # Wykona sie dla SCATTER, CHASE, oraz FRIGHTENED, kiedy zlapal go PacMan
                #                    if self.inTunnel: self.inTunnel = False
                self.possible = []
                for i in range(len(graph[self.e_Node])):
                    if graph[self.e_Node][i][0] != self.s_Node:
                        if (self.e_Node not in (18, 19, 41, 42)) or ((self.e_Node in (18, 19, 41, 42)) and (
                                graph[self.e_Node][i][0] not in (27, 28, 47, 48))):
                            if graph[self.e_Node][i][2] == NORTH:
                                self.possible.append((graph[self.e_Node][i][0], self.toTarget(
                                    [coords[self.e_Node][0], coords[self.e_Node][1] - gridSize]),
                                                      graph[self.e_Node][i][2], i))
                            elif graph[self.e_Node][i][2] == SOUTH:
                                self.possible.append((graph[self.e_Node][i][0], self.toTarget(
                                    [coords[self.e_Node][0], coords[self.e_Node][1] + gridSize]),
                                                      graph[self.e_Node][i][2], i))
                            elif graph[self.e_Node][i][2] == WEST:
                                self.possible.append((graph[self.e_Node][i][0], self.toTarget(
                                    [coords[self.e_Node][0] - gridSize, coords[self.e_Node][1]]),
                                                      graph[self.e_Node][i][2], i))
                            elif graph[self.e_Node][i][2] == EAST:
                                self.possible.append((graph[self.e_Node][i][0], self.toTarget(
                                    [coords[self.e_Node][0] + gridSize, coords[self.e_Node][1]]),
                                                      graph[self.e_Node][i][2], i))
                SelectionSort(self.possible, 2)
                SelectionSort(self.possible, 1)  # Nie chce mi sie
                if not self.wasFrightened and not self.wasTouched and (self.s_Node == 34 and self.e_Node == 35) or (
                        self.s_Node == 39 and self.e_Node == 38): self.velocity = SPEED  # tu bylo *=2, btw jesli bug z Frightenedem to pewnie tu
                # for i in range(len(graph[self.s_Node])):
                # if graph[self.s_Node][i][0] == self.e_Node:
                # if graph[self.s_Node][i][2]==NORTH or graph[self.s_Node][i][2]==SOUTH
                # self.orientation = graph[self.s_Node][i][2]
                # break
                self.s_Node = self.e_Node
                self.e_Node = self.possible[0][0]
                self.to_Node = graph[self.s_Node][self.possible[0][3]][1] * gridSize
                if self.possible[0][2] == NORTH:
                    # if self.orientation!=NORTH:
                    self.orientation = NORTH
                    if not self.wasTouched: self.animStep = 2
                    self.vec[0] = 0
                    self.vec[1] = -1
                elif self.possible[0][2] == SOUTH:
                    # if self.orientation[1]!=SOUTH:
                    self.orientation = SOUTH
                    if not self.wasTouched: self.animStep = 3
                    self.vec[0] = 0
                    self.vec[1] = 1
                elif self.possible[0][2] == WEST:
                    # if self.orientation[0]!=WEST:
                    if not self.wasTouched: self.animStep = 1
                    self.orientation = WEST
                    self.vec[0] = -1
                    self.vec[1] = 0
                elif self.possible[0][2] == EAST:
                    # if self.personality==BLINKY: print(self.orientation, self.possible[0][2])
                    # if self.orientation[0]!=EAST:
                    self.orientation = EAST
                    if not self.wasTouched: self.animStep = 0
                    self.vec[0] = 1
                    self.vec[1] = 0
                if (self.s_Node == 35 and self.e_Node == 34) or (self.s_Node == 38 and self.e_Node == 39):
                    self.velocity = SPEED / 2  # bylo *=1/2
                    # print(self.velocity)


# ********************************************************************************
#                               MAIN
# ********************************************************************************
# TODO: frightened mode (done), zmiana trybow(done), cornering (done), wizualne duszki, poziomy(done), smierc pac-mana(done), wizualna otoczka
clock = pygame.time.Clock()
GameChoice = None
# key = pygame.key.get_pressed()
genCoords(0, 1, 31)
# print(coords[18])

for i in range(0, len(coords)):
    coords[i][0] *= gridSize
    coords[i][1] *= gridSize

    MenuAnimationStep = 0
    MenuPosition = 0
    MenuCounter = 0
    MenuAim = GAMEON
    MenuPointer = pygame.image.load("graphics/PacMan4.png")
    anim1 = [pygame.transform.flip(pygame.image.load("graphics/PacMan.png"), True, False),
             pygame.transform.flip(pygame.image.load("graphics/PacMan2.png"), True, False),
             pygame.transform.flip(pygame.image.load("graphics/PacMan3.png"), True, False)]
    anim2 = [pygame.image.load("graphics/Blinky1.png"), pygame.image.load("graphics/Pinky1.png"),
             pygame.image.load("graphics/Inky1.png"), pygame.image.load("graphics/Clyde1.png")]

#SPEED = 4
while GameChoice!=EXIT:
    GameInit()
    pygame.mixer.music.play(-1)
    while GameChoice==None:
        for event in pygame.event.get():
            if(event.type==pygame.QUIT):
                    pygame.quit()
            if(event.type == pygame.KEYDOWN):
                if(event.key==pygame.K_s or event.key==pygame.K_DOWN):
                    if MenuAim == GAMEON:
                        MenuAim = HELP
                    elif MenuAim == HELP:
                        MenuAim = EXIT
                elif(event.key==pygame.K_w or event.key==pygame.K_UP):
                    if MenuAim == EXIT:
                        MenuAim = HELP
                    elif MenuAim == HELP:
                        MenuAim = GAMEON
                elif(event.key==pygame.K_RETURN):
                    GameChoice = MenuAim
            #if(event.type == pygame.KEYDOWN):
                #if event.key==
        screen.fill((0,0,0)) #notka do mnie 0 - czarny, 255 - bialy
        screen.blit(logo, (0,16))
        screen.blit(anim1[MenuAnimationStep], (MenuPosition-16, 150))
        screen.blit(anim2[0], (MenuPosition-80, 150))
        screen.blit(anim2[1], (MenuPosition-128, 150))
        screen.blit(anim2[2], (MenuPosition-176, 150))
        screen.blit(anim2[3], (MenuPosition-224, 150))
        screen.blit(button1, (0,200))
        screen.blit(button3, (0,300))
        screen.blit(button2, (0,400))
        screen.blit(MenuPointer, (32, MenuAim))
        screen.blit(MenuPointer, (bg_size[2]-64, MenuAim))
        MenuCounter+=1
        if MenuCounter == 3: 
            MenuAnimationStep+=1
            MenuCounter=0
        MenuAnimationStep%=3
        MenuPosition += 1
        #print(bg_size[3])
        if MenuPosition==bg_size[2]+224: MenuPosition=0
        pygame.display.update()
        clock.tick(FPS)
    while GameChoice==GAMEON and deathBuffer[2]>0: #Ogolem to deathbuffer zrobilem zanim nauczylem sie uzywac zmiennych globalnych, ale nie robi to za duzej roznicy wiec zostawiam tak 
        PSCounter = 0
        GSCounter = 0
        BSCounter = 0 #Blinky to jedyny duch o zmiennej predkosci w obrebie jednej tury
        if LEVEL > 1 and LEVEL < 5:
            inkyWaitTill = 0
            youAreUselessClyde=50
            if LEVEL>2: youAreUselessClyde=0
            frightime = 3*FPS
            mod2 = 974 
            mod3 = 5*60
            PSModifier = 9 #Bedzie gubil co 10. klatke (90% predkosci)
            GSModifier = 4 # 80% predkosci
        elif LEVEL >= 5 and LEVEL <22: 
            frightime = 2*FPS
            mod1 = -2
            mod2 = 978
            PSModifier = 0 #(100% predkosci)
            GSModifier = 19  
        elif LEVEL>21:
            PSModifier = 9 
            GSModifier = 19 # Skipuje co 20 klatke, 95% predkosci, at this point sa szybsze niz PM
    
        PM = PacMan(deathBuffer, "graphics/PacMan.png", "graphics/PacMan2.png", "graphics/PacMan3.png")
        BonusPosition = PM.position
        alreadyEaten=PM.eaten #Potrzebne do okreslenia momentu wyjscia z ghosthouse'u poszczegolnych duchow... Z poczatku at least
        if alreadyEaten==0: 
            WhereToFind = Starter.copy() #CZEMU KURWAAAAAAAAAAAAAAAAAAAAAAAA
            WhereToFind[(52,60)]=[1,2,1]
            WhereToFind[(59,65)]=[1,2,1]   
            #print(WhereToFind)
            #print(Starter)
            initPoints(0) #PM startuje kazdy poziom z 0 na eaten. Wiec jest to rownoznaczne z nowym poziomem.
        
        Blinky = Ghost(BSModifier,"graphics/Blinky1.png","graphics/Blinky4.png", "graphics/Blinky2.png", "graphics/Blinky3.png", BLINKY)
        Pinky = Ghost(GSModifier,"graphics/Pinky1.png","graphics/Pinky4.png","graphics/Pinky2.png","graphics/Pinky3.png", PINKY)
        Inky = Ghost(GSModifier,"graphics/Inky1.png","graphics/Inky4.png","graphics/Inky2.png","graphics/Inky3.png", INKY)
        Clyde = Ghost(GSModifier,"graphics/Clyde1.png","graphics/Clyde4.png","graphics/Clyde2.png","graphics/Clyde3.png", CLYDE)
        ALLGHOSTS = [Blinky, Pinky, Inky, Clyde] #Do zmiany trybu wszystkich duchow
        
        #modeTimer=FPS*(2*20+2*(7+mod1)+5+mod2)
        key = None
        NewCounter = 0
        pygame.mixer.music.stop()
        pygame.mixer.Sound.play(startsound)
        while NewCounter!=FPS*4:
            for event in pygame.event.get():
                if(event.type==pygame.KEYDOWN):
                    #print(PM.score)
                    if(event.key==pygame.K_ESCAPE): pygame.quit()
            setscr()
            Blinky.draw()
            Inky.draw()
            Pinky.draw()
            Clyde.draw()
            screen.blit(MenuPointer, PM.position)
            screen.blit(greeting, (PM.position[0]-16,PM.position[1]-82))
            pygame.display.update()
            NewCounter+=1
            clock.tick(FPS)

        while PM.alive:
            for event in pygame.event.get():
                if(event.type==pygame.QUIT):
                    #print(PM.score)
                    pygame.quit()
                if(event.type == pygame.KEYDOWN):
                    if(event.key==pygame.K_r or event.key==pygame.K_BACKQUOTE or event.key==pygame.K_q or event.key==pygame.K_e or event.key==pygame.K_SPACE):
                        PM.keyCallback(event.key, -200) #Do obslugi cheatow, pomagaja w debugowaniu ky... I wygrywaniu...
                    else: 
                        key = event.key
                else: key=None
            #Problem z frightened modem, a raczej z wyjsciem zen BUG
            #if pygame.KEYDOWN: key = pygame.key.get_pressed()   #Do mnie: Nie ruszaj tego pod zadnym kurwa pozorem, trust yourself
            if PSModifier and PSCounter == PSModifier: #Jesli PSModifier = 0 to nie bedziee zadnego spowolnienia 
                PM.wait+=1
                PSCounter = 0
            setscr() #ten uklad moze powodowac minimalne opoznienie w wizualizacji jedzenia kropek, ale to kwestia jednej klatki
            #if PM.eaten
            if PM.wait==0:
                PM.nodification(key)
                PM.move(animStep, key)
                PM.eat()
                if PSModifier: PSCounter+=1
            else:
                PM.draw(animStep)
                PM.wait-=1
        
            if Blinky.mode!=FRIGHTENED and Inky.mode!=FRIGHTENED and Pinky.mode!=FRIGHTENED and Clyde.mode!=FRIGHTENED:
                if modeTimer==FPS*(7+mod1):
                    setGhostMode(ALLGHOSTS, CHASE)
                    #print([Blinky.mode, Pinky.mode, Inky.mode, Clyde.mode])
                elif modeTimer==FPS*(20+7+mod1):
                    setGhostMode(ALLGHOSTS, SCATTER)
                elif modeTimer==FPS*(20+2*(7+mod1)):
                    setGhostMode(ALLGHOSTS, CHASE)
                elif modeTimer==FPS*(2*20+2*(7+mod1)):
                    setGhostMode(ALLGHOSTS, SCATTER)
                elif modeTimer==FPS*(2*20+2*(7+mod1)+5):
                    setGhostMode(ALLGHOSTS, CHASE)
                elif modeTimer==FPS*(2*20+2*(7+mod1)+5+mod2):
                    setGhostMode(ALLGHOSTS, SCATTER)
                elif modeTimer==FPS*(2*20+2*(7+mod1)+5+mod2)+int((5*FPS)/mod3):
                    setGhostMode(ALLGHOSTS, CHASE)
                    #print("Ja jebie")
                
                if modeTimer<=FPS*(2*20+2*(7+mod1)+5+mod2)+int((5*FPS)/mod3): modeTimer+=1
            
            else:
                if frightenedTimer == frightime or LEVEL>=21:
                    setGhostMode(ALLGHOSTS, CHASE)
                    frightenedTimer = 0
                else: 
                    if frightenedTimer>int(frightime/1.5) and frightenedTimer%10==0:
                        if not Pinky.wasTouched:
                            if Pinky.animStep==4:
                                Pinky.animStep=5
                            elif Pinky.animStep==5:
                                Pinky.animStep=4
                        if not Clyde.wasTouched:
                            if Clyde.animStep==4:
                                Clyde.animStep=5
                            elif Clyde.animStep==5:
                                Clyde.animStep=4
                        if not Blinky.wasTouched:
                            if Blinky.animStep==4:
                                Blinky.animStep=5
                            elif Blinky.animStep==5:
                                Blinky.animStep=4
                        if not Inky.wasTouched:
                            if Inky.animStep==4:
                                Inky.animStep=5
                            elif Inky.animStep==5:
                                Inky.animStep=4
                    frightenedTimer+=1
            
            if BSCounter == Blinky.SpeedModifier:
                Blinky.wait+=1
                BSCounter = 0
                
            if GSCounter == GSModifier:
                Inky.wait+=1
                Pinky.wait+=1
                Clyde.wait+=1
                GSCounter = 0
                
            if Blinky.wait == 0: 
                Blinky.nodification()
                Blinky.move(0,True, True)
                BSCounter += 1
                
            else:
                Blinky.draw()
                Blinky.wait -=1
                
            if Pinky.wait == 0:
                Pinky.nodification()
                Pinky.move(0,True,True)
                #print(Pinky.to_Node)
                Inky.nodification()
                Inky.move(0,Pinky.out and ((PM.eaten-alreadyEaten)>=inkyWaitTill), Pinky.out and PM.eatTimer>=4*FPS)
                Clyde.nodification()
                Clyde.move(0,Inky.out and PM.eaten-alreadyEaten>=youAreUselessClyde, Inky.out and PM.eatTimer>=4*FPS)        
                GSCounter += 1 #Starczy zwiekszyc to raz wiec nie musi sie pojawiac u innych duchow
            else: 
                Pinky.draw()
                Pinky.wait -= 1
                Inky.wait -=1
                Inky.draw()
                Clyde.wait -=1
                Clyde.draw()
            
            if not Clyde.out: PM.eatTimer+=1
            pygame.display.update()
            if PM.alive == False:
                deathBuffer = [PM.eaten, PM.score, PM.lives-1]
                #print(PM.lives)
                fade = pygame.Surface((bg_size[2],bg_size[3]))
                if deathBuffer[2]==0: 
                    GameChoice = None
                    print(PM.score, int(oof[0]))
                    if PM.score > int(oof[0]):
                        bestscore.seek(0)
                        bestscore.write(str(PM.score))
                        bestscore.truncate()
                    deathscreen = pygame.image.load("graphics/DeathScreen.png").convert()
                    fade.set_alpha(0)
                    pygame.mixer.Sound.play(wasted)
                    for alpha in range(0,100):
                        fade.fill((0,0,0))
                        fade.set_alpha(alpha/2)
                        screen.blit(fade, (0,0))
                        deathscreen.set_alpha(alpha)
                        screen.blit(deathscreen, (0, bg_size[3]/2-80))
                        pygame.display.update()
                        pygame.time.delay(80)
                        #clock.tick(FPS)
                else:
                    pygame.mixer.Sound.play(r_oof)
                    fade.fill((255,0,0))
                    for alpha in range(0,30):
                        fade.set_alpha(alpha)
                        screen.blit(fade, (0,0))
                        pygame.display.update()
                        pygame.time.delay(20)
                    #pygame.time.delay(100)
            if(PM.canMove==True and counter==2): 
                animStep += 1
                counter = 0
            if(PM.canMove==True): animStep %= 3
            if(PM.canMove==True): counter+=1
            clock.tick(FPS)
            #print(PM.eaten,    amount)
            if PM.eaten == amount+8:
                LEVEL+=1
                deathBuffer = [0, PM.score, PM.lives]
                del PM
                del Blinky
                del Pinky
                del Inky
                del Clyde
                break
bestscore.close()
pygame.quit()